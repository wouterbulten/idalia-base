# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant::Config.run do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  ## Ubuntu 12.04 LTS (32-bit)
  config.vm.box = "precise32"
  config.vm.box_url = "http://files.vagrantup.com/precise32.box"

  ## Ubuntu 12.04 LTS (64-bit)
  # config.vm.box = "precise64"
  # config.vm.box_url = "http://files.vagrantup.com/precise64.box"

  ## Ubuntu 12.10 LTS (64-bit)
  #config.vm.box = "quantal64"
  #config.vm.box_url = "https://github.com/downloads/roderik/VagrantQuantal64Box/quantal64.box"

  # Boot with a GUI so you can see the screen. (Default is headless)
  # config.vm.boot_mode = :gui

  # Share an additional folder to the guest VM. The first argument is
  # an identifier, the second is the path on the guest to mount the
  # folder, and the third is the path on the host to the actual folder.
  config.vm.share_folder("v-application", "/vagrant/application", "./application", :nfs => true)
  config.vm.share_folder("v-phpMyAdmin", "/vagrant/phpMyAdmin", "./tools/phpMyAdmin", :nfs => true)

  # Set time zone
  config.vm.provision :shell, :inline => "echo \"Europe/London\" | sudo tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata"

  # Enable PHP 5.4 (comment this out to use Ubuntu's default: which is probably 5.3)
  # config.vm.provision :shell, :inline => "apt-get install -y python-software-properties && add-apt-repository ppa:ondrej/php5"
 
  # Update the server
  config.vm.provision :shell, :inline => "apt-get update --fix-missing"

  # Enable provisioning with Puppet stand alone.  Puppet manifests
  # are contained in a directory path relative to this Vagrantfile.
  # You will need to create the manifests directory and a manifest in
  # the file base.pp in the manifests_path directory.

  ###
  #  Creates a basic LAMP stack with MySQL using PUPPET
  #  
  #  To launch run: vagrant up dev_puppet
  ###
  config.vm.define :dev_puppet do |default_config|

    # Map development box to this IP
    config.vm.network :hostonly, "33.33.33.10"

    # Enable Puppet
    default_config.vm.provision :puppet do |puppet|
      puppet.facter = { "fqdn" => "dev.idalia", "hostname" => "www" } 
      puppet.manifests_path = "puppet/manifests"
      puppet.manifest_file  = "idalia-development.pp"
      puppet.module_path  = "puppet/modules"
    end

  end

  ###
  #  Creates a basic LAMP stack with MySQL using CHEF SOLO
  #  
  #  To launch run: vagrant up dev_chef
  ###
  config.vm.define :dev_chef do |chef_config|

    # Map development box to this IP
    config.vm.network :hostonly, "33.33.33.10"


    config.vm.provision :chef_solo do |chef|
      chef.add_recipe "apt"
      chef.add_recipe "openssl"
      chef.add_recipe "apache2"
      chef.add_recipe "mysql"
      chef.add_recipe "mysql::server"
      chef.add_recipe "php"
      chef.add_recipe "php::module_apc"
      chef.add_recipe "php::module_curl"
      chef.add_recipe "php::module_mysql"
      chef.add_recipe "apache2::mod_php5"
      chef.add_recipe "apache2::mod_rewrite"
      chef.json = {
          :mysql => {
              :server_root_password => 'root',
              :bind_address => '127.0.0.1'
          }
      }
    end
  end

  # config.vm.provision :puppet, :module_path => "puppet/modules"


  # Enable provisioning with chef solo, specifying a cookbooks path, roles
  # path, and data_bags path (all relative to this Vagrantfile), and adding 
  # some recipes and/or roles.
  #
  # config.vm.provision :chef_solo do |chef|
  #   chef.cookbooks_path = "../my-recipes/cookbooks"
  #   chef.roles_path = "../my-recipes/roles"
  #   chef.data_bags_path = "../my-recipes/data_bags"
  #   chef.add_recipe "mysql"
  #   chef.add_role "web"
  #
  #   # You may also specify custom JSON attributes:
  #   chef.json = { :mysql_password => "foo" }
  # end

  # Enable provisioning with chef server, specifying the chef server URL,
  # and the path to the validation key (relative to this Vagrantfile).
  #
  # The Opscode Platform uses HTTPS. Substitute your organization for
  # ORGNAME in the URL and validation key.
  #
  # If you have your own Chef Server, use the appropriate URL, which may be
  # HTTP instead of HTTPS depending on your configuration. Also change the
  # validation key to validation.pem.
  #
  # config.vm.provision :chef_client do |chef|
  #   chef.chef_server_url = "https://api.opscode.com/organizations/ORGNAME"
  #   chef.validation_key_path = "ORGNAME-validator.pem"
  # end
  #
  # If you're using the Opscode platform, your validator client is
  # ORGNAME-validator, replacing ORGNAME with your organization name.
  #
  # IF you have your own Chef Server, the default validation client name is
  # chef-validator, unless you changed the configuration.
  #
  #   chef.validation_client_name = "ORGNAME-validator"
end
