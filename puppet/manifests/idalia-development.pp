#
# Idalia Development Configuration
#
# Apache, PHP 5, MySQL
#

include apache
include php
include mysql

$docroot 		= '/vagrant/application/public/'
$phpMyAdmin 	= '/vagrant/phpMyAdmin/'

# Apache setup
class {'apache::mod::php': }

apache::vhost { 'local.dev':
	servername => '33.33.33.10',
	priority => '20',
	port => '80',
	docroot => $docroot,
	configure_firewall => false,
}

a2mod { 'rewrite': ensure => present; }

# PHP Extensions
php::module { ['xdebug', 'mysql', 'curl', 'gd'] : 
    notify => [ Service['httpd'], ],
}
php::conf { [ 'pdo', 'pdo_mysql']:
    require => Package['php5-mysql'],
    notify  => Service['httpd'],
}

# MySQL Server
class { 'mysql::server':
  config_hash => { 'root_password' => 'fet5aFre' }
}

mysql::db { 'idaliadb':
    user     => 'idalia',
    password => 'localdev',
    host     => 'localhost',
    grant    => ['all'],
    charset => 'utf8',
    require => File['/root/.my.cnf'],
}

# Other Packages
$extras = ['vim', 'curl', 'phpunit']
package { $extras : ensure => 'installed' }
